import praw, webbrowser
from review_bot.config import config

if __name__ == '__main__':
    reddit = praw.Reddit(config.REDDIT_USER_AGENT)
    reddit.set_oauth_app_info(
        client_id=config.REDDIT_CLIENT_ID,
        client_secret=config.REDDIT_CLIENT_SECRET,
        redirect_uri=config.REDDIT_REDIRECT_URI
        )
    state = 'WeDontReallyCare'
    scopes = ['submit', 'privatemessages', 'identity']
    url = reddit.get_authorize_url(state, ' '.join(scopes), True)
    webbrowser.open(url)

    code = input('Authorization code: ')
    access_info = reddit.get_access_information(code)
    print(access_info)