from review_bot import ReviewBot
from review_bot.config import config
import logging
import os

if __name__ == '__main__':
    pid = os.getpid()
    with open('review-bot.pid', 'w') as pfile:
        pfile.write(str(pid))
    logging.basicConfig(
        filename=config.LOG_FILE,
        format=config.LOG_FORMAT,
        datefmt=config.LOG_DATEFORMAT,
        style=config.LOG_FORMAT_STYLE,
        level=config.LOG_LEVEL
    )
    requests_logger = logging.getLogger('requests')
    requests_logger.propagate = False

    logging.info('Starting bot using config: ' + config.__name__)
    bot = ReviewBot()
    while True:
        bot.browse()
