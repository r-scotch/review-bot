import os, logging

# Reddit Config
REDDIT_USER_AGENT = ''
REDDIT_CLIENT_ID  = ''
REDDIT_CLIENT_SECRET = ''
REDDIT_REDIRECT_URI  = ''
REDDIT_REFRESH_TOKEN = ''

# API Config
API_URL = 'https://reviewapi.janssen.io'
API_USERNAME = ''
API_PASSWORD = ''

# Logger Config
LOG_FILE = os.path.join('logs', 'api.debug.log')
LOG_LEVEL = logging.DEBUG
LOG_FORMAT = '{asctime} | {levelname:^8} | {message}'
LOG_DATEFORMAT = '%Y-%m-%d %H:%M:%S'
LOG_FORMAT_STYLE = '{'

# App Config
SLEEP_TIME = 60

# Redis Config
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
REDIS_PASSWORD = None
