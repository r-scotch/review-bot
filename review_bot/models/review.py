import requests

class Review:
    def __init__(self, review_id, bottle_id, bottle_name, reviewer_id, reviewer_name, link, rating, price, date, title, subreddit):
        self.review_id   = review_id
        self.bottle_id   = bottle_id
        self.bottle      = bottle_name
        self.reviewer_id = reviewer_id
        self.reviewer    = reviewer_name
        self.link        = link
        self.rating      = rating
        self.price       = price
        self.date        = date
        self.title       = title
        self.subreddit   = subreddit

    def __repr__(self):
        return '<Review: {}>'.format(self.review_id)

    def __str__(self):
        return 'Review by {} about "{}".'.format(self.reviewer, self.bottle)

    @staticmethod
    def from_dict(review_dict):
        return Review(
            review_id = review_dict['id'],
            bottle_id = review_dict['bottle_id'],
            bottle_name = review_dict['bottle'],
            reviewer_id = review_dict['reviewer_id'],
            reviewer_name = review_dict['reviewer'],
            link = review_dict['link'],
            rating = review_dict['rating'],
            price = review_dict['price'],
            date = review_dict['date'],
            title = review_dict['title'],
            subreddit = review_dict['subreddit']
            )