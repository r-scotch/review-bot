class Response:
    template = '{header}{body}{footer}'

    def __init__(self, header='', footer='', delimiter='\n'):
        self.parts = []
        self.header = header
        self.footer = footer
        self.delimiter = delimiter

    def __repr__(self):
        return '<Response: size {}>'.format(len(self.parts))

    def __str__(self):
        return self.build()

    def build(self):
        header = ''
        if self.header:
            header = self.header + self.delimiter
        footer = ''
        if self.footer:
            footer = self.delimiter + self.footer
        body = self.delimiter.join(self.parts)
        return Response.template.format(
            header = header,
            body   = body,
            footer= footer
        )

    def add(self, part):
        self.parts.append(part)

    def is_empty(self):
        return len(self.parts) == 0

    def clear(self):
        self.parts = []
