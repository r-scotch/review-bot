from fuzzywuzzy import fuzz

def distinct(collection, attribute):
    distinct_collection = []
    distinct_attributes = set()
    for item in collection:
        attribute_value = getattr(item, attribute)
        if not attribute_value in distinct_attributes:
            distinct_attributes.add(attribute_value)
            distinct_collection.append(item)
    return distinct_collection

def approximate_filter(bottle, reviews, threshold=75):
    matches = []
    bottle = bottle.lower()
    for review in reviews:
        title_ratio = fuzz.partial_ratio(bottle, review.title.lower())
        bottle_ratio = fuzz.partial_ratio(bottle, review.bottle.lower())
        ratio = max(title_ratio, bottle_ratio)
        if ratio > threshold:
            matches.append( (review, ratio) )

    sorted_matches = sorted(matches, key=lambda x: x[1], reverse=True)
    return [item[0] for item in sorted_matches]

def get_approximate_bottle(bottle, reviewer, repository):
    limit = 500
    offset = 0
    reviews = []
    api_reviews = repository.get_reviews_by_reviewer(reviewer, limit, offset)
    reviews.extend(api_reviews)
    while len(api_reviews) > 0:
        offset += limit
        api_reviews = repository.get_reviews_by_reviewer(reviewer, limit, offset)
        reviews.extend(api_reviews)
    return approximate_filter(bottle, reviews)

