from time import time
import logging
import pickle
from redis import StrictRedis
from review_bot.config import config

logger = logging.getLogger('cache')

class TimedCache:
    def __init__(self, expires_in):
        self.expires_in = expires_in
        self.redis = StrictRedis(
            host=config.REDIS_HOST,
            port=config.REDIS_PORT,
            password=config.REDIS_PASSWORD
        )

    def __call__(self, func):
        def wrapped_func(*args, **kwargs):
            wrapped_func.__name__ = func.__name__
            key = self.hash_args(args, kwargs)
            # prepend function name to make sure each function uses unique keys
            key = func.__name__ + str(key)
            value = self.redis.get(key)
            if value:
                value = pickle.loads(value)
            else:
                logger.debug('Refreshing cache for "{}"'.format(key))
                value = func(*args, **kwargs)
                self.redis.setex(key, self.expires_in, pickle.dumps(value))
            return value
        return wrapped_func

    def hash_args(self, args, kwargs):
        kw_tuple = tuple(sorted(kwargs.items()))
        return args + kw_tuple
