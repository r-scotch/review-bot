import json, requests, logging
from review_bot.config import config
from review_bot.helpers.exceptions import AuthenticationError
from requests.exceptions import ConnectionError
from requests.auth import HTTPBasicAuth
from requests.auth import AuthBase

logger = logging.getLogger('auth')

class TokenStore:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.token = None

    def get(self):
        if self.token:
            return self.token
        self.refresh()
        return self.token

    def refresh(self):
        ''' Stores the JWT token in the instance.
        Raises: review_bot.AuthenticationError if request fails.
        '''
        logger.debug('Getting new token for `{}`'.format(self.username))
        try:
            response = requests.get(config.API_URL + '/auth/token',
                auth=HTTPBasicAuth(self.username, self.password)
            )
        except ConnectionError:
            # Connection is vital to the application. No use to just log it and proceed.
            raise AuthenticationError(404)
        if response.status_code == 200:
            content = response.content.decode('utf-8')
            self.token = json.loads(content)['token']
        else:
            raise AuthenticationError(response.status_code)

class BearerTokenAuth(AuthBase):
    def __init__(self, token_store):
        self.token_store = token_store
        self.retry = True

    def handle_expired_token(self, response, **kwargs):
        if response.status_code != 401:
            return response
        try:
            content = response.content.decode('utf-8')
            error = json.loads(content)
        except ValueError:
            response.raise_for_status()
        else:
            if error.get('reason', None) == 'expired token':
                if not self.retry:
                    logger.critical('Token seems to expire immediately.')
                    return response
                logging.debug('Retrying request.')
                return self.retry_request(response, kwargs)

    def retry_request(self, response, kwargs):
        ''' Retries the request of a given response by refreshing the token
            and setting the Authorization header again.
            [Based on code from requests.auth.HTTPDigestAuth]
        '''   
        # Don't retry the request another time
        self.retry = False
        # Grab a new token
        self.token_store.refresh()
        # Consume content and release the original connection
        # to allow our new request to reuse the same one.
        response.content
        response.close()
        new_request = response.request.copy()
        self.add_headers(new_request)
        new_response = response.connection.send(new_request, **kwargs)
        new_response.history.append(response)
        new_response.request = new_request

        return new_response

    def add_headers(self, request):
        request.headers['Authorization'] = 'Bearer {token}'.format(
            token=self.token_store.get()
        )

    def __call__(self, request):
        request.register_hook('response', self.handle_expired_token)
        self.add_headers(request)
        return request