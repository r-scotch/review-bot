class AuthenticationError(Exception):
    def __init__(self, status):
        status = str(status)
        Exception.__init__(self, 
            'Authentication failed. Response has status <{}>'
            .format(status)
        )
