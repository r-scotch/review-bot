def escape(string):
    # courtesy of markdown module v2.6.6
    escaped_chars = ['\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '>', '#', '+', '-', '.', '!']
    for esc_char in escaped_chars:
        string = string.replace(esc_char, '\\' + esc_char)
    return string