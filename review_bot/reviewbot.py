from review_bot.config import config
from review_bot.helpers import auth, Response
from review_bot.repositories import ReviewRepository
from review_bot.helpers import TokenStore
from review_bot.helpers import markdown
from review_bot.helpers import data
import re, time, praw, logging, requests

logger = logging.getLogger('ReviewBot')

class ReviewBot:
    def __init__(self, reddit=None):
        self.reddit = reddit or self.login()
        self.token_store = TokenStore(config.API_USERNAME, config.API_PASSWORD)
        self.token_store.refresh()
        self.review_repo = ReviewRepository(50, self.token_store)

        reply_footer = "___\n^(Hey, I'm just a simple bot. If you have any remarks, please direct them at /u/FlockOnFire)"
        self.response = Response(footer=reply_footer, delimiter='\n\n')

        username = self.reddit.user.name
        self.trigger_expressions = {
            'latest': re.compile(r'/u/{name} latest'.format(name=username), re.I),
            'subreddit': re.compile(r'/u/{name} /r/([a-z_-]+)'.format(name=username), re.I),
            'bottle': re.compile(r'''/u/{name} ([`'"])([a-z0-9' _-]+?)\1'''.format(name=username), re.I)
        }

    def login(self):
        '''
        Login to Reddit using information from the config file.
        Returns:
            praw.Reddit instance
        '''
        logger.info('Logging into reddit. User-agent: ' + config.REDDIT_USER_AGENT)
        reddit = praw.Reddit(config.REDDIT_USER_AGENT)
        reddit.set_oauth_app_info(
            client_id=config.REDDIT_CLIENT_ID,
            client_secret=config.REDDIT_CLIENT_SECRET,
            redirect_uri=config.REDDIT_REDIRECT_URI
        )
        reddit.refresh_access_information(config.REDDIT_REFRESH_TOKEN)
        return reddit

    def browse(self):
        ''' Do whatever the bot needs to do '''
        try:
            self.process_messages()
        except requests.exceptions.ConnectionError as error:
            logging.error('Failed to establish connection, sleeping for 10min')
            time.sleep(600)    
        except praw.errors.HTTPException as error:
            status = error._raw.status_code
            if 500 <= status < 600:
                logging.error(error, exc_info=True)
                time.sleep(600)
            else:
                logging.error(error, exc_info=True)
                raise # re-raise exception
        except ConnectionError as error:
            logging.error('Failed to connect, sleeping for a while')
            time.sleep(600)
        except TypeError as error:
            logging.error(error)
            time.sleep(600)            
        except Exception as error:
            logging.error(error, exc_info=True)
            raise
        time.sleep(config.SLEEP_TIME)

    def process_messages(self):
        '''
        Reply to unread messages.
        Returns:
            None
        '''
        for message in self.reddit.get_unread():
            self.build_latest_response(message)
            self.build_subreddit_response(message)
            self.build_bottle_response(message)
            if not self.response.is_empty():
                logger.info('Replying to /u/{}: {}'.format(
                    message.author.name, repr(self.response)
                ))
                message.reply(str(self.response))
                self.response.clear()
            message.mark_as_read()

    def build_latest_response(self, message):
        '''
        Check message for a `latest`-trigger and add the latest
        10 reviews from distinct submissions to the response.
        Parameters:
            message: PRAW Message object.
        Returns:
            (list) Seperate parts (strings) to add to a response.
        '''
        username = message.author.name
        text = message.body
        if self.trigger_expressions['latest'].search(text):
            logger.debug('Found latest trigger.')
            self.response.add("/u/{}'s latest reviews:".format(username))
            reviews = self.review_repo.get_reviews_by_reviewer(username)
            if reviews:
                reviews = data.distinct(reviews, 'title')[:10] # get first ten distinct links
                self.response.add(self.format_reviews(reviews))
            else:
                self.response.add('* No reviews yet.')

    def build_subreddit_response(self, message):
        '''
        Check message for one or multiple `subreddit`-triggers and add the latest
        10 reviews from distinct submissions from the subreddit(s) to the response.
        Parameters:
            message: PRAW Message object.
        Returns:
            None
        '''
        username = message.author.name
        text = message.body
        subreddits = self.trigger_expressions['subreddit'].findall(text)
        if not subreddits:
            return None

        for sub in subreddits:
            logger.debug('Found subreddit trigger.')
            reviews = self.review_repo.get_reviews_by_reviewer_and_subreddit(username, sub)
            self.response.add("/u/{}'s latest reviews in /r/{}:".format(username, sub))
            if reviews:
                reviews = data.distinct(reviews, 'title')[:10] # get first ten distinct links
                self.response.add(self.format_reviews(reviews))
            else:
                self.response.add('* I could not find any of your reviews in this subreddit.')

    def build_bottle_response(self, message):
        '''
        Check message for one or multiple `bottle`-triggers and add the latest
        10 reviews from distinct submissions about the bottle(s) to the response.
        Parameters:
            message: PRAW Message object.
        Returns:
            None
        '''
        username = message.author.name
        text = message.body
        bottles = self.trigger_expressions['bottle'].findall(text)
        if not bottles:
            return None

        for bottle in bottles:
            bottle = bottle[1]
            logger.debug('Found bottle trigger.')
            reviews = data.get_approximate_bottle(bottle, username, self.review_repo)
            self.response.add("/u/{}'s latest reviews about *{}*:".format(username, bottle))
            if reviews:
                reviews = data.distinct(reviews, 'title')[:10]
                self.response.add(self.format_reviews(reviews))
            else:
                self.response.add('* I could not find any of your reviews about this bottle.')

    def format_reviews(self, reviews):
        '''
        Formats a list of reviews according to a template.
        Parameters:
            reviews: list of Review objects
        Returns:
            (string) formatted reviews
        '''
        response_template = '* {rating}/100 - [{title}]({link})\n'
        formatted = ''
        for review in reviews:
            formatted += response_template.format(
                rating=review.rating,
                title=markdown.escape(review.title),
                link=review.link
            )
        return formatted
