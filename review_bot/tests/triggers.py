from review_bot.tests.mocks import Message, User, Reddit
from review_bot import ReviewBot
import pdb

def create_bot():
    user = User('review_bot')
    reddit = Reddit(user)
    return ReviewBot(reddit)

def create_message(filename):
    author = User('ReviewBotTest')
    with open(filename, 'r') as f:
        text = f.read()
    return Message(author=author, body=text)

def test_latest_trigger():   
    author = User('ReviewBotTest')
    with open('review_bot/tests/data/list_by_user.txt', 'r') as f:
        text = f.read()
    message = Message(author=author, body=text)
    bot = create_bot()
    match = bot.trigger_expressions['latest'].search(text)
    assert match

def test_subreddit_trigger():
    subreddit_trigger_test('review_bot/tests/data/list_by_subreddit.1.txt', 'scotch')
    subreddit_trigger_test('review_bot/tests/data/list_by_subreddit.2.txt', 'whiskeyreviews')
    subreddit_trigger_test('review_bot/tests/data/list_by_subreddit.3.txt', 'worldwhisky')

def subreddit_trigger_test(filename, subreddit_name):
    message = create_message(filename)
    bot = create_bot()
    match = bot.trigger_expressions['subreddit'].findall(message.body)
    assert match
    assert len(match) == 1
    assert match[0].lower() == subreddit_name.lower()

def test_bottle_trigger():
    bottle_trigger_test('review_bot/tests/data/list_by_bottle.1.txt', 'talisker 10')
    bottle_trigger_test('review_bot/tests/data/list_by_bottle.2.txt', '2014 Old Forester Birthday Bourbon')
    bottle_trigger_test('review_bot/tests/data/list_by_bottle.3.txt', "Aberlour 15 Cuvee Marie d'Ecosse")

def bottle_trigger_test(filename, bottle_name):
    message = create_message(filename)
    bot = create_bot()
    match = bot.trigger_expressions['bottle'].findall(message.body)
    assert match
    assert len(match) == 1
    _, bottle = match[0]
    assert bottle.lower() == bottle_name.lower()