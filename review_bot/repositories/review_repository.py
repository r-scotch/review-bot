from review_bot.repositories import Repository
from review_bot.helpers.auth import BearerTokenAuth
from review_bot.helpers.cache import TimedCache
from review_bot.config import config
from review_bot.models import Review

import requests
import logging
import json

logger = logging.getLogger('ReviewRepo')

class ReviewRepository(Repository):
    def __init__(self, result_limit, token_store):
        self.result_limit = result_limit
        super().__init__(token_store)

    @TimedCache(300)
    def get_reviews_by_reviewer(self, reviewer, limit=None, offset=0):
        '''
        Get latest reviews from a specific reviewer
        Parameters:
            reviewer: (string) Reddit username of the reviewers
            limit: (optional int) Maximum numbers of reviews to fetch.
                Default: global limit of the instance
        Returns:
            List of Review objects
        '''
        if not limit:
            limit = self.result_limit
        url = '{base}/api/reviewers/{reviewer}/reviews/?limit={limit}&offset={offset}'.format(
            base     = config.API_URL,
            reviewer = reviewer,
            limit    = limit,
            offset   = offset
        )
        response = requests.get(url, auth=BearerTokenAuth(self.token_store))
        if response.status_code == 200:
            data = self.response_json(response)
        elif response.status_code == 404:
            data = []
        else:
            response.raise_for_status()
        return [Review.from_dict(rev_dict) for rev_dict in data]

    @TimedCache(300)
    def get_reviews_by_reviewer_and_bottle(self, reviewer, bottle, limit=None):
        '''
        NB: Not implemented yet
        Get latest reviews from a specific reviewer about a specific bottle
        Parameters:
            reviewer: (string) Reddit username of the reviewers
            bottle: (string) name of the bottle
            limit: (optional int) Maximum numbers of reviews to fetch.
                Default: global limit of the instance
        Returns:
            List of Review objects
        '''
        raise NotImplementedError
        if not limit:
            limit = self.result_limit

    @TimedCache(300)
    def get_reviews_by_reviewer_and_subreddit(self, reviewer, subreddit, limit=None):
        '''
        Get latest reviews from a specific reviewer in a specific subreddit
        Parameters:
            reviewer: (string) Reddit username of the reviewers
            bottle: (string) name of the subreddit
            limit: (optional int) Maximum numbers of reviews to fetch.
                Default: global limit of the instance
        Returns:
            List of Review objects
        '''
        limit = limit or self.result_limit
        url = '{base}/api/reviewers/{reviewer}/reviews/by_subreddit/{subreddit}?limit={limit}'.format(
            base      = config.API_URL,
            reviewer  = reviewer,
            subreddit = subreddit,
            limit     = limit
        )
        response = requests.get(url, auth=BearerTokenAuth(self.token_store))
        if response.status_code == 200:
            data = self.response_json(response)
        elif response.status_code == 404:
            data = []
        else:
            response.raise_for_status()
        return [Review.from_dict(rev_dict) for rev_dict in data]

    @TimedCache(300)
    def get_reviews(self, limit=None):
        '''
        Get latest reviews.
        Parameters:
            limit: (optional int) Maximum numbers of reviews to fetch.
                Default: global limit of the instance
        Returns:
            List of Review objects
        '''
        if not limit:
            limit = self.result_limit
        url = '{base}/api/reviews/?limit={limit}'.format(
            base  = config.API_URL,
            limit = limit
        )
        response = requests.get(url, auth=BearerTokenAuth(self.token_store))
        if response.status_code == 200:
            data = self.response_json(response)
        elif response.status_code == 404:
            data = []
        else:
            response.raise_for_status()
        return [Review.from_dict(rev_dict) for rev_dict in data]
