import requests, json
from review_bot.config import config
from review_bot.helpers import auth

class Repository:
    '''
    Repository base class contains logic to request data 
    from the API using HTTP Basic Authentication and JWT
    '''
    def __init__(self, token_store):
        '''
        Repository initializer
        Parameters:
            token: (review_bot.TokenStore) Instance containing the JWT Token.
        '''
        self.token_store = token_store        

    def response_json(self, response):
        '''
        Load the content of the JSON response.
        Raises:
            ValueError: if the content is not in valid JSON.
        Returns:
            Python dictionary representation of the JSON response content.
        '''
        content = response.content.decode('utf-8')
        return json.loads(content)

